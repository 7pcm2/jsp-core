package com.jspiders.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.jspiders.restaurant.constant.EntityConstant;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
@Entity
@Table(name = EntityConstant.RESTURANT_INFO)
public class ResturantInfo implements Serializable {

	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private Long  id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "menu")
	private String menu;
	
	@Column(name = "rating")
	private String raitng;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "pin_code")
	private String pinCode;
	
	private String country;
	
	public ResturantInfo() {
	
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getRaitng() {
		return raitng;
	}

	public void setRaitng(String raitng) {
		this.raitng = raitng;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ResturantInfo [id=" + id + ", name=" + name + ", address=" + address + ", menu=" + menu + ", raitng="
				+ raitng + ", type=" + type + ", pinCode=" + pinCode + "]";
	}
	
	
	
}
