package com.jspiders.restaurant.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.restaurant.entity.Mobile;
import com.jspiders.restaurant.entity.Person;
import com.jspiders.restaurant.util.SessionFactoryUtil;

public class AssosiationRepository {

	public void savePersonDetails(Person person) {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(person);
		transaction.commit();
	}
	
	public void saveMobileDetails(Mobile mobile) {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(mobile);
		transaction.commit();
	}
}




