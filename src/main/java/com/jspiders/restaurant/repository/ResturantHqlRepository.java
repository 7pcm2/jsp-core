package com.jspiders.restaurant.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.jspiders.restaurant.dto.ResturantDto;
import com.jspiders.restaurant.entity.ResturantInfo;
import com.jspiders.restaurant.util.SessionFactoryUtil;

public class ResturantHqlRepository {

	
	public List<ResturantInfo> findAll() {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("from ResturantInfo");
		
		Query query = session.createQuery(stringBuilder.toString());
		return query.getResultList();
	}
	
	public List<ResturantInfo> findByResturant(String resturantName) {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("from ResturantInfo");
		stringBuilder.append(" where name=:rn");
		
		//stringBuilder.append("'");
		//stringBuilder.append(resturantName);
		//stringBuilder.append("'");
		
		Query query = session.createQuery(stringBuilder.toString());
		query.setParameter("rn", resturantName);
		return query.getResultList();
	}
	
	private void updateResturantDetails(ResturantDto resturantDto) {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("update ResturantInfo set address=:a,pinCode=:p where id=:i");
		
		Query query = session.createQuery(stringBuilder.toString());
		query.setParameter("a", resturantDto.getAddress());
		query.setParameter("p", resturantDto.getPinCode());
		query.setParameter("i", resturantDto.getId());
		
		Transaction transaction = session.beginTransaction();
		int executeUpdate = query.executeUpdate();
		transaction.commit();
	}
	
	public void deleteByResturantName(String resturantName) {
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("delete from ResturantInfo");	
		stringBuilder.append(" where name=:n");
		
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery(stringBuilder.toString());
		query.setParameter("n", resturantName);
		int deletedRows = query.executeUpdate();
		transaction.commit();
	}
	
	
}





















