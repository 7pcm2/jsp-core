package com.jspiders.restaurant.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.jspiders.restaurant.entity.AdharDetails;
import com.jspiders.restaurant.entity.Application;
import com.jspiders.restaurant.entity.Mobile;
import com.jspiders.restaurant.entity.Person;
import com.jspiders.restaurant.entity.ResturantInfo;

public class SessionFactoryUtil {

	private static SessionFactory sessionFactory = null;
	
	public static SessionFactory getSessionFactoryObject() {
		if( sessionFactory == null) {
		    Properties properties = PropertiesUtil.getConnectionProperties();
		    Configuration cfg = new Configuration();
		    cfg.setProperties(properties);
		    cfg.addAnnotatedClass(ResturantInfo.class);
		    cfg.addAnnotatedClass(Person.class);
		    cfg.addAnnotatedClass(AdharDetails.class);
		    cfg.addAnnotatedClass(Mobile.class);
		    cfg.addAnnotatedClass(Application.class);
			try {
				sessionFactory = cfg.buildSessionFactory();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
